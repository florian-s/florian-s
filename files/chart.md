
```mermaid
flowchart LR
    root(Web stack)
    root-->Frontend
    root-->Backend
    subgraph Backend [" "]
    back(Back)-->node[Node.js]
    node-->express[Express.js]
    node--and the outsider-->astro[Astro]
    back-->python(Python)
    python--best framework-->django[Django]
    python--for APIs-->drf[Django Rest Framework]
    back-->java(Java)
    java-->spring[Spring Boot]
    end
    node--with some-->typescript[Typescript]
    js--with some-->typescript
    subgraph Frontend [" "]
    front[Front]-->html(Modern HTML and CSS)
    front-->js(Vanilla Javascript)
    js--when needed-->react[React]
    end
```
