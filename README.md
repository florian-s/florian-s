# Florian S

> Jump to <a href="https://gitlab.com/users/florian-s/projects">my projects</a> if you don't want to read this intro.

Hi, I am Florian and I am a software engineer working as a Fullstack web developer. Currently living in Paris 🇫🇷  
I used to be a consultant, mostly doing agile product management.

You should check out <a href="https://github.com/florian-s-code">my Github</a> too to find my older projects.

Beyond development, I have had the opportunity to manage some infrastructures (Linux + Docker) for student organizations. 

I 💙 open source software, I use Linux (Archlinux) and I am a loyal Firefox user.


## Favorite tech

I have been programming in Javascript since forever and learned backend programming with Python + Django.  
Professionally I now develop in Java + Spring Boot as well as in ReactJS.

I strive for simplicity and love vanilla CSS over any CSS-in-JS or CSS framework. Always ready to look for an advanced CSS3 selector that will make the JS simpler.


 ## Current interest

I ❤️ Jamstack and I currently use <a href="https://astro.build">Astro</a> as my tool of choice. It can also do SSR, so what is not to love.
